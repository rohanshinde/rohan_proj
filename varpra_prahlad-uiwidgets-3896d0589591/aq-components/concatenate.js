const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
const files = [
'./dist/aq-components/runtime.js',
'./dist/aq-components/polyfills.js',
//'./dist/aq-components/scripts.js',
'./dist/aq-components/main.js',
]
await fs.ensureDir('elements')
await concat(files, 'elements/aq-components.js');
await fs.copyFile('./dist/aq-components/styles.css', 'elements/styles.css')
//await fs.copy('./dist/angular-elements/assets/', 'elements/assets/' )
})()