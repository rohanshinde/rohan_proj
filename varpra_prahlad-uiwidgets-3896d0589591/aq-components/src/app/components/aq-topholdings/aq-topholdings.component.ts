import { Component, Input, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { Account, AccountList } from 'src/app/models/accountList'
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'custom-aq-topholdings',
  templateUrl: './aq-topholdings.component.html',
  styleUrls: ['./aq-topholdings.component.css']
})
export class AqTopholdingsComponent implements OnInit {
  @Input() total:number;

  accounts: Account[];

  arrayItems: {
    SYMBOL: string;
    DESCRIPTION: string;
    QUANTITY:number;
    MARKETVALUE:number; 
    FINICKNAME:string;
    ACCOUNTCLASSIFICATION:string;
  }[];

  finicknametoshow:string;
  accountclassificationtoshow:string;

  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) { 
    this.total=0;
    this.accounts = [];
    this.arrayItems = [];
    this.finicknametoshow="";
    this.accountclassificationtoshow="";
  }

  ngOnInit(): void {
    console.log("Inside Init");
    console.log("Inside Init total is"+this.total);
    this.GetAccounts();
  }
  editClick(finickname:string,accountclassification:string){  
    this.finicknametoshow=finickname;
    this.accountclassificationtoshow=accountclassification;
  }
  closeClick(){
  }
  private GetAccounts() {
    console.log("Inside GetAccounts");
    this.SpinnerService.show();
    //let sessionid = this._repository.getSessionid();
    //let token = this._repository.getToken();

    let sessionid = "57324d564c704479505355355963704b426c366874413d3d";
    let token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTg4ODk1NjksImV4cCI6MTYxODg5MzE2OSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiIxQTYzREI5ODI3OTQ2QkYxRUQ2Mzg2MzlGODhBMDUyNCIsImlhdCI6MTYxODg4OTU2OSwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.UhkVzgmYQ9uh-Jcm6JfkadyfuPpgkaj2_SvuTSBmDAT3L4Rjsz1Hwq_UqX7yBSD34N5ok1Puo3_mXTutcDjcsGrjAmjldeeW4QrYhjomjFbjOyZ4_vKYtna2vB7U0_W5rY8Ufs9HJs6tkkSu8-MONqpk4W5nAbw-i4YfrSSRJX9_inaCboOVqv7SfC2AOEkCG5c-iDq6PMr_XUS7sFkkFfF-z5X8zGHCPYTb-QFBTj7E5B0naHqYLcFYELAHQqIUVEJGKjEBL90q07vlSGBhNJdIdsDQL8IOyqAn23QymJcGDHsEfTCfNFKOeacb72BoyMASEg-J5loX-fG6pfveVw";
   
    this._http.getAccounts(token, sessionid).subscribe(data => {
      this.ProcessAccounts(data);
      this.SpinnerService.hide();
    });
  }
  private ProcessAccounts(data: AccountList) {
    console.log("Inside Processaccounts");
    this.accounts = data.Accounts;

    //Get all positions arryas of each account   
    for (let i = 0; i < this.accounts.length; i++) {
      var PositionsAary = this.accounts[i].Positions;
      var finickname=this.accounts[i].FINickName;
      var accountclassification=this.accounts[i].AccountClassification;
      if(PositionsAary!=null)
      {
        for (let k = 0; k < PositionsAary.length; k++) {  
                
          this.arrayItems.push({ SYMBOL: PositionsAary[k].Ticker, DESCRIPTION:PositionsAary[k].Description , QUANTITY:PositionsAary[k].Quantity,MARKETVALUE:  PositionsAary[k].MarketValue,FINICKNAME:finickname,ACCOUNTCLASSIFICATION:accountclassification});
        }
      }
    }
    console.log(this.arrayItems);

    //sort array by descending order by market value    
    this.arrayItems.sort(function(a, b){
      return b.MARKETVALUE - a.MARKETVALUE;
    });

    //if total is greater then zero..Display rows eqal to total
    if(this.total>0)
    {
      var tempArrayItem=this.arrayItems;
      this.arrayItems = [];
      for (let j = 0; j < this.total; j++) {
        var yy =tempArrayItem[j];
        this.arrayItems.push(yy);
      }  

    }
    console.log(this.arrayItems);

  }
}
