import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AqTopholdingsComponent } from './aq-topholdings.component';

describe('AqTopholdingsComponent', () => {
  let component: AqTopholdingsComponent;
  let fixture: ComponentFixture<AqTopholdingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AqTopholdingsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AqTopholdingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
