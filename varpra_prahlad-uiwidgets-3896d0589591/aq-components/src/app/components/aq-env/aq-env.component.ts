import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { HttpService } from '../../Services/http.service';
import { ValueRepositoryService } from '../../Services/value-repository.service';

@Component({
  selector: 'custom-aq-env',
  templateUrl: './aq-env.component.html',
  styleUrls: ['./aq-env.component.css']
})
export class AqEnvComponent implements OnInit {
  @Input() _token:string;
  @Input() _sessionid:string;
  
  constructor(private _repository:ValueRepositoryService, private _http:HttpService) {
    this._token="";
    this._sessionid="";
    console.log("Inside Constructor");
   }

  
   @Input() set token(value: string) {  
    this._token=value;  
   //this._repository.setToken(value);
   console.log("Token set successfully"+this._repository.getToken());
   
 }
 @Input() set sessionid(value: string) {  
   this._sessionid=value;  
   //this._repository.setSessionid(value);
   console.log("Session set successfully"+this._repository.getSessionid());
 }

  ngOnInit(): void {  
    console.log("Inside Init"+this._sessionid) 
    this.keepAliveSession();
  }

  keepAliveSession():void{
    console.log("Inside keep alive seeion"+this._repository.getSessionid());
    console.log("Inside keep alive token"+this._repository.getToken());
    setInterval(() => {
      let sessionid=this._repository.getSessionid();
      this._http.keepAliveSession(this.token,this.sessionid).subscribe(data=>{
        console.log(data);
      });
    }, 60000);
    
  }
}
