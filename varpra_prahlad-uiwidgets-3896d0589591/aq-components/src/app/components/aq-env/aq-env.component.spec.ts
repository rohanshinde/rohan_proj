import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AqEnvComponent } from './aq-env.component';

describe('AqEnvComponent', () => {
  let component: AqEnvComponent;
  let fixture: ComponentFixture<AqEnvComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AqEnvComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AqEnvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
