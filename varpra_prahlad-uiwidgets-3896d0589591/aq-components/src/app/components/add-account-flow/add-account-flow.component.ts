import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AccountToken } from 'src/app/models/accountList';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTc2MjIzNjYsImV4cCI6MTYxNzYyNTk2NiwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiIxMkE2ODRBNEMzMDgzMDRENkM1NERCOUUwMzU1MUIxMSIsImlhdCI6MTYxNzYyMjM2NSwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.Ho1PU_8VCvsBD92HZRe1wV2TXG_UqN49PC5_gc1EZEgqbqSyiwAODEwAxX_ADQVCGYSUt2_259BI4gX-AYOjg8B-xq9foSMnn4J2_g_fqYeTNyxZZszdGdhqtRhk7cIOAcUqn5EFESwtifMLsJWsDqt3hOtLlMclqrXGB-EsvgNtouxem1729TKY7ipt8FYbxUe2Dv9eX2Qoq9i-FOEVnxghYxa9EPC-N0IVR0NO4h_nXodtW4InU-7MHwPDNLBhhz-e2VNsASocxxsE_VV6fse00FecsHUdEqBXVefi5GHRNiawgtTr1OFpT4SeZj6bZ73U1ha9zA05BEOaKu7Lyw";

@Component({
  selector: 'add-account-flow',
  templateUrl: './add-account-flow.component.html',
  styleUrls: ['./add-account-flow.component.css']
})
export class AddAccountFlowComponent implements OnInit {
  divshowFISearch: boolean = true;
  divshowEnterCredentials: boolean = false; 
  divshowAccountClassification:boolean=false;
  divshowAccountStatus:boolean=false;
  divshowChallengeQuestion:boolean=false;

  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) { }

  ngOnInit(): void {

    this._repository.StepChangeSubscriber.subscribe(data=>{
      if(data){
        console.log(data);    
        console.log("Step is --->"+this._repository.getStep());
        if(this._repository.getStep()==='2'){
         this.divshowFISearch=false;
         this.divshowEnterCredentials=true;
        }
        if(this._repository.getStep()==='1'){
          this.divshowFISearch=true;
          this.divshowEnterCredentials=false;
        }
      }
    
    });

    this._repository.AccountTokenChangeSubscriber.subscribe(data=>{
      console.log(data);   
      console.log("Inside account tokenn change subscriber..");  
    
      switch (this._repository.getaccounttoken().ProcessCode) {
        case '101':
          console.log("Process code is 101 send request for process code 101");
          this.AddAccount();
          break;       
        case '102':
          this.divshowFISearch = false;
          this.divshowEnterCredentials = false; 
          this.divshowAccountClassification=true;
          this.divshowAccountStatus=false;
          this.divshowChallengeQuestion=false;
          console.log("Process code is 102 send request for process code 102");
          this._repository.harvestfetchaccount=this._repository.getaccounttoken().HarvestFetchAccounts.slice(1);
          break;
        case '104':
          this.divshowFISearch = false;
          this.divshowEnterCredentials = true; 
          this.divshowAccountClassification=false;
          this.divshowAccountStatus=true;   
          this.divshowChallengeQuestion=false;       
          break;      
        case '106':
          console.log("Process code is 106 send request for process code 106");
          this.AddAccount();
          break;
        case '1053':
          this.divshowFISearch = false;
          this.divshowEnterCredentials = false; 
          this.divshowAccountClassification=false;
          this.divshowAccountStatus=false;
          this.divshowChallengeQuestion=true;
          console.log("Process code is 1053 send request for process code 1053 Challenge Question..");
          this._repository.challengequestions=this._repository.getaccounttoken().ChallengeQuestions.slice(1); 
          break;
        case '000':
          this.divshowFISearch = false;
          this.divshowEnterCredentials = false; 
          this.divshowAccountClassification=false;
          this.divshowAccountStatus=true;
          this.divshowChallengeQuestion=false;
          console.log("Process code is 000 Please contact support");
          this._repository.AddAccountStatusMessage=this._repository.getaccounttoken().ExtendedErrorMessage;           
          break;
        case '999':
            this.divshowFISearch = false;
            this.divshowEnterCredentials = false; 
            this.divshowAccountClassification=false;
            this.divshowAccountStatus=true;
            this.divshowChallengeQuestion=false;
            console.log("Process code is 999 Account Added Successfully..!!!");
            this._repository.AddAccountStatusMessage="Success.";           
          break;
       }
    });
  }

  AddAccount(){
    this._http.addAccount(token,this._repository.getaccounttoken()).subscribe(data => {
      console.log("*********************Inside account initiate for code 101 or 106..****************************");
      console.log("Harvest Id is.."+data.HarvestID);
      console.log("process code is.."+data.ProcessCode);
      console.log("process code is.."+JSON.stringify(data.HarvestFetchAccounts));
     this._repository.setaccounttoken(data);
     this.SpinnerService.hide();  
  });
  }

}
