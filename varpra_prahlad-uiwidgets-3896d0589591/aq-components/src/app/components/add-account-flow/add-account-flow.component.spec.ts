import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccountFlowComponent } from './add-account-flow.component';

describe('AddAccountFlowComponent', () => {
  let component: AddAccountFlowComponent;
  let fixture: ComponentFixture<AddAccountFlowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAccountFlowComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccountFlowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
