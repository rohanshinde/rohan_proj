import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccountAccountclassificationComponent } from './add-account-accountclassification.component';

describe('AddAccountAccountclassificationComponent', () => {
  let component: AddAccountAccountclassificationComponent;
  let fixture: ComponentFixture<AddAccountAccountclassificationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAccountAccountclassificationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccountAccountclassificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
