import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTgzMTYwNTgsImV4cCI6MTYxODMxOTY1OCwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJEQUQyOUQ1QTkyMUM1QkE1OUI1MjMzQTAxOEU3NEY1RSIsImlhdCI6MTYxODMxNjA1Niwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.BNkgrS7Nno5NmxsCsoXEa62gcz9UZnUaGxrtDA_yqU2rlE2ZbSPmJ4BOn826_FpkDl3KMT2DyF3VSSTN3HRa70HWj5eDPxWm_vB1N8SeztjOMQ7VKO8fKQ-HT-dfHH8MDzJawSHjcwoAdYhvtNRzPQ64XYVDr4lTzjFe0pDNEzRmZg2CybQkoMfNtDYCJWE3PM8XDu_5LJoV4tXdm4ubuzpKkcH-dgSP6esmhmVQgob3gjISiPGl4Z9KNsXV_5X2aiz4So-jBTxrhMcVgm6uXvRx8VRowyGjPitF4CrI-O8PiDuoxzDhL3ZKNNsmu6Y3tFLUCV8emJuD6XF-sHeGVg";
const session="714730434a4b63446a412b562f5636642b69686b51513d3d";

@Component({
  selector: 'custom-add-account-accountclassification',
  templateUrl: './add-account-accountclassification.component.html',
  styleUrls: ['./add-account-accountclassification.component.css']
})
export class AddAccountAccountclassificationComponent implements OnInit {
  form: FormGroup;

  AccountType: any[];
  HarvestFetchAccounts:any[];

  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService,public fb: FormBuilder) { 
    this.form = this.fb.group({
      harvestaccounts: this.fb.array([], [Validators.required])
    })

    //Get Account Type from repository
    this.AccountType=_repository.getfiaccountdatalist();
    //Get Harvest Account from repository
    this.HarvestFetchAccounts=_repository.getharvestfetchaccount();  
  }

  ngOnInit(): void {
  }

  onSubmit() {
    var accounttoken=this._repository.getaccounttoken();
    accounttoken.HarvestFetchAccounts=[];

    this.SpinnerService.show();
    const harvestaccounts: FormArray = this.form.get('harvestaccounts') as FormArray;    
    for (let i = 0; i < harvestaccounts.length; i++) {      
      const element = harvestaccounts.at(i);    
      let obj = this.HarvestFetchAccounts.find(o => o.NickNameAtFI === element.value);     
      console.log("object is"+JSON.stringify(obj));
      accounttoken.HarvestFetchAccounts.push(obj);
    }

   // var accounttoken=this._repository.getaccounttoken();
    //console.log("Account token is"+accounttoken);
    //accounttoken.HarvestFetchAccounts=this._repository.getaccounttoken().HarvestFetchAccounts.slice(1);

    this._http.addAccount(token,accounttoken).subscribe(data => {
      console.log("*********************Inside account Classification ..****************************");
      console.log("Harvest Id is.."+data.HarvestID);
      console.log("process code is.."+data.ProcessCode);
      console.log("process code is.."+JSON.stringify(data.HarvestFetchAccounts));
     this._repository.setaccounttoken(data);
     this.SpinnerService.hide();  
  });
   
  }

  onCheckboxChange(e:any) {
    const harvestaccounts: FormArray = this.form.get('harvestaccounts') as FormArray;
  
    if (e.target.checked) {
      harvestaccounts.push(new FormControl(e.target.value));
      console.log(e.target.value);
    } else {
       const index = harvestaccounts.controls.findIndex(x => x.value === e.target.value);
       harvestaccounts.removeAt(index);
    }
  }
}
