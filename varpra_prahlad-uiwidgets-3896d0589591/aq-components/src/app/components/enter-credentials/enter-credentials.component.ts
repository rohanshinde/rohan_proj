import { Component,Input, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { AccountToken,FILoginParameter, ParameterList } from 'src/app/models/accountList'
import { NgxSpinnerService } from "ngx-spinner";

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTgzMTYwNTgsImV4cCI6MTYxODMxOTY1OCwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJEQUQyOUQ1QTkyMUM1QkE1OUI1MjMzQTAxOEU3NEY1RSIsImlhdCI6MTYxODMxNjA1Niwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.BNkgrS7Nno5NmxsCsoXEa62gcz9UZnUaGxrtDA_yqU2rlE2ZbSPmJ4BOn826_FpkDl3KMT2DyF3VSSTN3HRa70HWj5eDPxWm_vB1N8SeztjOMQ7VKO8fKQ-HT-dfHH8MDzJawSHjcwoAdYhvtNRzPQ64XYVDr4lTzjFe0pDNEzRmZg2CybQkoMfNtDYCJWE3PM8XDu_5LJoV4tXdm4ubuzpKkcH-dgSP6esmhmVQgob3gjISiPGl4Z9KNsXV_5X2aiz4So-jBTxrhMcVgm6uXvRx8VRowyGjPitF4CrI-O8PiDuoxzDhL3ZKNNsmu6Y3tFLUCV8emJuD6XF-sHeGVg";
const session="714730434a4b63446a412b562f5636642b69686b51513d3d";

@Component({
  selector: 'custom-enter-credentials',
  templateUrl: './enter-credentials.component.html',
  styleUrls: ['./enter-credentials.component.css']
})


export class EnterCredentialsComponent implements OnInit {
  filoginParameters:FILoginParameter[];  
  parameterlist:ParameterList[];
  FInstitutionName:string;
   

  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) { 
    this.filoginParameters=[];
    this.parameterlist=[];
    this.FInstitutionName="";
  }
 

  ngOnInit(): void {

    this.SpinnerService.show();
    console.log("parameters are -->"+JSON.stringify(this.filoginParameters));     
    console.log("Inside enter credentials");    
    //this._repository.FIIDChangeSubscriber.subscribe(data=>{
     // console.log("Inside change subscriber..");
     // if(data){       
       //console.log(data);    
       console.log("FIID is --->"+this._repository.getFIID());
       this._http.getFinancialInstitution(token,this._repository.getFIID()).subscribe(data => { 
        this.FInstitutionName=data.FinancialInstitution.FIName;      
        this.filoginParameters = data.FinancialInstitution.FILoginParameters;   
        this._repository.fiaccountdatalist=data.FinancialInstitution.FiAccountDataList;
        console.log("parameters are sub -->"+JSON.stringify(this.filoginParameters));  
        
        console.log("Stepis in fiid change sub->"+this._repository.getStep());   
        
        this.SpinnerService.hide();  
       //});  
      //}
    });
    console.log("parameters are out sub -->"+JSON.stringify(this.filoginParameters));  
  }

  
 
  Login() {
    //Generate parameter list
    this.SpinnerService.show();
    this.parameterlist=[];
    for (let i = 0; i < this.filoginParameters.length; i++) {    
    this.parameterlist.push({ParamName:this.filoginParameters[i].ParameterId.toString(),
                             ParamVal:this.filoginParameters[i].ParameterDefaultValue}); 
    }
    console.log("Parameter list is--->"+JSON.stringify(this.parameterlist));
    //Initiate add accounts
    //CashEdge Bank - Retail Non MFA
    this.InitiateAddAccount();
  }
  goBack(){
    this._repository.setStep('1');  
  }

  InitiateAddAccount(){
   
    this._http.addAccountInitiate(token,this._repository.getFIID(),session,this.parameterlist).subscribe(data => {
         console.log("*********************Inside account initiate..****************************");
         console.log("Harvest Id is.."+data.HarvestID);
         console.log("process code is.."+data.ProcessCode);
         console.log("process code is.."+JSON.stringify(data.HarvestFetchAccounts));
        this._repository.setaccounttoken(data);
        this.SpinnerService.hide();  
     });
  }
}
