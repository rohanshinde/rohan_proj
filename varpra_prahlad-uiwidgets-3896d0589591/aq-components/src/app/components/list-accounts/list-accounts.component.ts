import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { Account, AccountList } from 'src/app/models/accountList'
import { NgxSpinnerService } from "ngx-spinner";
import * as _ from 'lodash';

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTkxNTA2MjEsImV4cCI6MTYxOTE1NDIyMSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJBRUUxREM2NzFFMUZERUI3RkM3MERFOTdBQTY1RUYzRSIsImlhdCI6MTYxOTE1MDYyMSwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.Av2whkHFA69tbqiBp0YrdSrM_tyVujACVvC7nGUp_ry6fBJD_ok5TymB37lciVp3cBVDxMq1RbGO2vuptBC3m30Yu58XFT3jueTTbRfW-c8bus3bOfRq0artsr4EG8W6M8qCegvDMKo5Q4oeYa-X032AujLNJ54tm3CjfJRlefDEjfxmIbYomXV_eM8jzX4m963Y1VpfcQn17W8rM3eoV_ZGFbcZAsM8Cwt1bq5eiY2L-yQusDjpB9OCNzbIo_eL5rrgr_Mep_vkpSRQWpvxpKERpF9J4GJ6ynJcELDYHWvRFoXtF6fP16UYm56aYrtpvy6xv_uuAtkfjymaH-jdbA";
const sessionid="71684b2f4833574177505765343152613478644565413d3d";


@Component({
  selector: 'list-accounts',
  templateUrl: './list-accounts.component.html',
  styleUrls: ['./list-accounts.component.css']
})
export class ListAccountsComponent implements OnInit {
 
  loadChildComponent:boolean=false;
  div1: boolean = true;
  div2: boolean = false;
  bntStyle = "tablinks active";
  bntStyle2 = "tablinks";

  accounts: Account[];
  AllAssetNodes: any;
  InvestmentNodesbyFInickname: any[];
  AccountClassificationarr: any;

  AllAssetNodesLib: any;
  InvestmentNodesbyFInicknameLib: any[];
  AccountClassificationarrLib: any;

  arrayItems: {
    HeaderValue: string;
    SumOFAmount: number;
    PushedItems: any[];
  }[];

  arrayItemsLib: {
    HeaderValue: string;
    SumOFAmount: number;
    PushedItems: any[];
  }[];

  fiidtoupdate:string;
  FInametoupdate:string;



  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) {
    this.accounts = [];

    this.AllAssetNodes = [];
    this.InvestmentNodesbyFInickname = [];
    this.AccountClassificationarr = [];
    this.arrayItems = [];

    this.AllAssetNodesLib = [];
    this.InvestmentNodesbyFInicknameLib = [];
    this.AccountClassificationarrLib = [];
    this.arrayItemsLib = [];

    this.fiidtoupdate="";
    this.FInametoupdate="";   
  }


  ngOnInit(): void {
    console.log("Inside Init list account");
    this.GetAccounts();
  }
  editClick(fiid:string,FIname:string){  
    this.fiidtoupdate=fiid;
    this.FInametoupdate=FIname;   
  }
  closeClick(){
  }
  ExternalLink(fiid:string){
    
    this._http.getFinancialInstitution(token,fiid).subscribe(data => { 
      let url=data.FinancialInstitution.Url;
      window.open(url, '_blank');
  });
  }
  UpdateFIName(){
    this._http.setClientFIMapping(token, "213510",this.fiidtoupdate,this.FInametoupdate).subscribe(data => {
      if(data){
        this.accounts = [];

    this.AllAssetNodes = [];
    this.InvestmentNodesbyFInickname = [];
    this.AccountClassificationarr = [];
    this.arrayItems = [];

    this.AllAssetNodesLib = [];
    this.InvestmentNodesbyFInicknameLib = [];
    this.AccountClassificationarrLib = [];
    this.arrayItemsLib = [];

        this.GetAccounts();
      }         
    });
  }
  private GetAccounts() {

    this.SpinnerService.show();
    //let sessionid = this._repository.getSessionid();
   // console.log("Inside List Accounts session is" + sessionid);
   // let token = this._repository.getToken();
  
    console.log("Inside List Accounts token is" + token);
    this._http.getAccounts(token, sessionid).subscribe(data => {
      this.ProcessAccounts(data);
      this.SpinnerService.hide();
    });
  }

  private ProcessAccounts(data: AccountList) {
    this.accounts = data.Accounts;
    console.log(this.accounts);
    
    let AllAssets = _.filter(this.accounts, function (acc) {
      return acc.AssetType === 'Assets';
    });
    let AllLiabilities = _.filter(this.accounts, function (acc) {
      return acc.AssetType === 'Liabilities';
    });

    this.AccountClassificationarr = this.uniqueBy(AllAssets, "AccountClassification");
    console.log(this.AccountClassificationarr);

    this.AccountClassificationarrLib = this.uniqueBy(AllLiabilities, "AccountClassification");
    console.log(this.AccountClassificationarrLib);

    for (let i = 0; i < this.AccountClassificationarr.length; i++) {
      let x = _.filter(this.accounts, (acc) => {
        return acc.AssetType === 'Assets' && acc.AccountClassification === this.AccountClassificationarr[i];
      });
      //console.log(x);
      this.AllAssetNodes.push(x);
    }
    console.log(this.AllAssetNodes);

    for (let i = 0; i < this.AccountClassificationarrLib.length; i++) {
      var x = _.filter(this.accounts, (acc) => {
        return acc.AssetType === 'Liabilities' && acc.AccountClassification === this.AccountClassificationarrLib[i];
      });
      //console.log(x);
      this.AllAssetNodesLib.push(x);
    }
    console.log(this.AllAssetNodesLib);

    for (let i = 0; i < this.AllAssetNodes.length; i++) {
      var FINickNamearr = this.uniqueBy(this.AllAssetNodes[i], "FINickName");
      var xx = this.AllAssetNodes[i];
      var sum = 0;
      var classificationtype;
      for (let k = 0; k < xx.length; k++) {
        sum += xx[k].TotalBalance;
        classificationtype = xx[k].AccountClassification;

      }
      console.log(sum);
      console.log(classificationtype);

      for (let j = 0; j < FINickNamearr.length; j++) {
        var y = _.filter(this.AllAssetNodes[i], function (acc) {
          return acc.FINickName === FINickNamearr[j];
        });


        this.InvestmentNodesbyFInickname.push(y);
        console.log(this.InvestmentNodesbyFInickname);
      }
      this.arrayItems.push({ HeaderValue: classificationtype, SumOFAmount: sum, PushedItems: this.InvestmentNodesbyFInickname });
      this.InvestmentNodesbyFInickname = [];
    }
    console.log(this.arrayItems);



    for (let i = 0; i < this.AllAssetNodesLib.length; i++) {
      var FINickNamearrLib = this.uniqueBy(this.AllAssetNodesLib[i], "FINickName");
      var xxx = this.AllAssetNodesLib[i];
      var sumLib = 0;
      var classificationtypeLib;
      for (let k = 0; k < xxx.length; k++) {
        sumLib += xxx[k].TotalBalance;
        classificationtypeLib = xxx[k].AccountClassification;

      }
      console.log(sumLib);
      console.log(classificationtypeLib);

      for (let j = 0; j < FINickNamearrLib.length; j++) {
        var y = _.filter(this.AllAssetNodesLib[i], function (acc) {
          return acc.FINickName === FINickNamearrLib[j];
        });


        this.InvestmentNodesbyFInicknameLib.push(y);
        console.log(this.InvestmentNodesbyFInicknameLib);
      }
      this.arrayItemsLib.push({ HeaderValue: classificationtypeLib, SumOFAmount: sumLib, PushedItems: this.InvestmentNodesbyFInicknameLib });
      this.InvestmentNodesbyFInicknameLib = [];
    }
    console.log(this.arrayItemsLib);
  }

  uniqueBy(arr: any, prop: any) {
    return arr.reduce((a: any, d: any) => {
      if (!a.includes(d[prop])) { a.push(d[prop]); }
      return a;
    }, []);
  }


  div1Function() {
    this.div1 = true;
    this.div2 = false;
    this.bntStyle = "tablinks active";
    this.bntStyle2 = "tablinks";
  }
  div2Function() {
    this.div2 = true;
    this.div1 = false;
    this.bntStyle = "tablinks";
    this.bntStyle2 = "tablinks active";
  }
  loadMyChildComponent(accid:string){
    this.loadChildComponent = true;    
    this._repository.setAccountID(accid);
  
 }

}

