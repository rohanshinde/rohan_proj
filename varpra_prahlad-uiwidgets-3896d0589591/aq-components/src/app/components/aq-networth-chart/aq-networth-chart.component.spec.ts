import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AqNetworthChartComponent } from './aq-networth-chart.component';

describe('AqNetworthChartComponent', () => {
  let component: AqNetworthChartComponent;
  let fixture: ComponentFixture<AqNetworthChartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AqNetworthChartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AqNetworthChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
