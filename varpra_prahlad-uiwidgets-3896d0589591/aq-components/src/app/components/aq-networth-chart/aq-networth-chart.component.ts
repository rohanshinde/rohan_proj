import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { MultiDataSet, Label,PluginServiceGlobalRegistrationAndOptions } from 'ng2-charts';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { Account, AccountList } from 'src/app/models/accountList'
import { NgxSpinnerService } from "ngx-spinner";

import * as _ from 'lodash';

@Component({
  selector: 'custom-aq-networth-chart',
  templateUrl: './aq-networth-chart.component.html',
  styleUrls: ['./aq-networth-chart.component.css']
})
export class AqNetworthChartComponent implements OnInit {

  accounts: Account[];
  AllAssetNodes: any;
  InvestmentNodesbyFInickname: any[];
  AccountClassificationarr: any;

  AllAssetNodesLib: any;
  InvestmentNodesbyFInicknameLib: any[];
  AccountClassificationarrLib: any;

  arrayItems: {
    HeaderValue: string;
    SumOFAmount: number;
    PushedItems: any[];
  }[];

  arrayItemsLib: {
    HeaderValue: string;
    SumOFAmount: number;
    PushedItems: any[];
  }[];

  displayNetworth:number;

  //Asset Chart variables

  assetChartLabels: Label[];
  assetChartData: MultiDataSet;
  assetChartType: ChartType = 'doughnut';
  assetSum:number=0;
  public pieChartColors: Array < any > = [{
    backgroundColor: ['#F47C2F', '#00A9C9', '#CACACA','#B0B91F','#e0b78d','#dd815f','#d43d51'],
    //borderColor: ['rgba(252, 235, 89, 0.2)', 'rgba(77, 152, 202, 0.2)', 'rgba(241, 107, 119, 0.2)','rgba(80, 107, 119, 0.2)']
  }];

  public centerText: String = "Center Text";
  public assetChartOption: ChartOptions ={
    legend: {
      position: 'right',
      labels: {
        fontSize: 12,
        usePointStyle: true,
        padding:20,        
      }
    },
    cutoutPercentage: 75,    
  }
  public assetChartPlugins: PluginServiceGlobalRegistrationAndOptions[] = [{  }];  

  //Liabilities Chart Variables
  liabilitiesChartLabels: Label[];
  liabilitiesChartData: MultiDataSet;
  liabilitiesChartType: ChartType = 'doughnut';
  liabilitiesSum:number;
  public liabilitiespieChartColors: Array < any > = [{
    backgroundColor: ['#b7cd8b', '#00A9C9', '#F47C2F','#B0B91F','#f4b976','#e97f57','#d43d51'],
   // borderColor: ['rgba(252, 235, 89, 0.2)', 'rgba(77, 152, 202, 0.2)', 'rgba(241, 107, 119, 0.2)','rgba(80, 107, 119, 0.2)']
  }];
  public liabilitiesChartOption: any = {
    legend: {
      position: 'right',
      labels: {
        fontSize: 12,
        usePointStyle: true,
        marginbottom:5,
        padding:20,
      }
    },
    cutoutPercentage: 75,
    
  }
  public liabilitiesChartPlugins: PluginServiceGlobalRegistrationAndOptions[] = [{ }];


  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) {
    this.accounts = [];

    this.AllAssetNodes = [];
    this.InvestmentNodesbyFInickname = [];
    this.AccountClassificationarr = [];
    this.arrayItems = [];

    this.AllAssetNodesLib = [];
    this.InvestmentNodesbyFInicknameLib = [];
    this.AccountClassificationarrLib = [];
    this.arrayItemsLib = [];

    this.assetChartLabels=[];
    this.assetChartData=[];
    this.assetSum=0;

    this.liabilitiesChartLabels=[];
    this.liabilitiesChartData=[];
    this.liabilitiesSum=0;
    this.displayNetworth=0;
   }
  ngOnInit(): void {
    this.GetAccounts();
  }
  private GetAccounts() {

    this.SpinnerService.show();
    //let sessionid = this._repository.getSessionid();   
    //let token = this._repository.getToken();
    let sessionid = "714730434a4b63446a412b562f5636642b69686b51513d3d";   
    let token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTgzMTYwNTgsImV4cCI6MTYxODMxOTY1OCwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJEQUQyOUQ1QTkyMUM1QkE1OUI1MjMzQTAxOEU3NEY1RSIsImlhdCI6MTYxODMxNjA1Niwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.BNkgrS7Nno5NmxsCsoXEa62gcz9UZnUaGxrtDA_yqU2rlE2ZbSPmJ4BOn826_FpkDl3KMT2DyF3VSSTN3HRa70HWj5eDPxWm_vB1N8SeztjOMQ7VKO8fKQ-HT-dfHH8MDzJawSHjcwoAdYhvtNRzPQ64XYVDr4lTzjFe0pDNEzRmZg2CybQkoMfNtDYCJWE3PM8XDu_5LJoV4tXdm4ubuzpKkcH-dgSP6esmhmVQgob3gjISiPGl4Z9KNsXV_5X2aiz4So-jBTxrhMcVgm6uXvRx8VRowyGjPitF4CrI-O8PiDuoxzDhL3ZKNNsmu6Y3tFLUCV8emJuD6XF-sHeGVg";

    console.log("Inside List Accounts token is" + token);
    this._http.getAccounts(token, sessionid).subscribe(data => {
      this.ProcessAccounts(data);
      this.SpinnerService.hide();
    });
  }
  private ProcessAccounts(data: AccountList) {
    this.accounts = data.Accounts;
    console.log(this.accounts);
    
    let AllAssets = _.filter(this.accounts, function (acc) {
      return acc.AssetType === 'Assets';
    });
    let AllLiabilities = _.filter(this.accounts, function (acc) {
      return acc.AssetType === 'Liabilities';
    });

    //following code will give ["Banking", "Insurance", "Investment", "Retirement"] i.e all unique classification array in assets
    this.AccountClassificationarr = this.uniqueBy(AllAssets, "AccountClassification");
    console.log(this.AccountClassificationarr);

    //following code will give ["Billing", "CreditCard", "OtherLiabilities", "Mortgage"]i.e all unique classification array in liabilities
    this.AccountClassificationarrLib = this.uniqueBy(AllLiabilities, "AccountClassification");
    console.log(this.AccountClassificationarrLib);

    for (let i = 0; i < this.AccountClassificationarr.length; i++) {
      let x = _.filter(this.accounts, (acc) => {
        return acc.AssetType === 'Assets' && acc.AccountClassification === this.AccountClassificationarr[i];
      });
      //console.log(x);
      this.AllAssetNodes.push(x);
    }
    console.log(this.AllAssetNodes);

    for (let i = 0; i < this.AccountClassificationarrLib.length; i++) {
      var x = _.filter(this.accounts, (acc) => {
        return acc.AssetType === 'Liabilities' && acc.AccountClassification === this.AccountClassificationarrLib[i];
      });
      //console.log(x);
      this.AllAssetNodesLib.push(x);
    }
    console.log(this.AllAssetNodesLib);

    for (let i = 0; i < this.AllAssetNodes.length; i++) {
      var FINickNamearr = this.uniqueBy(this.AllAssetNodes[i], "FINickName");
      var xx = this.AllAssetNodes[i];
      var sum = 0;
      var classificationtype;
      for (let k = 0; k < xx.length; k++) {
        sum += xx[k].TotalBalance;
        classificationtype = xx[k].AccountClassification;

      }
      console.log(sum);
      console.log(classificationtype);

      for (let j = 0; j < FINickNamearr.length; j++) {
        var y = _.filter(this.AllAssetNodes[i], function (acc) {
          return acc.FINickName === FINickNamearr[j];
        });


        this.InvestmentNodesbyFInickname.push(y);
        console.log(this.InvestmentNodesbyFInickname);
      }
      this.arrayItems.push({ HeaderValue: classificationtype, SumOFAmount: sum, PushedItems: this.InvestmentNodesbyFInickname });
      this.InvestmentNodesbyFInickname = [];
    }
    console.log(this.arrayItems);



    for (let i = 0; i < this.AllAssetNodesLib.length; i++) {
      var FINickNamearrLib = this.uniqueBy(this.AllAssetNodesLib[i], "FINickName");
      var xxx = this.AllAssetNodesLib[i];
      var sumLib = 0;
      var classificationtypeLib;
      for (let k = 0; k < xxx.length; k++) {
        sumLib += xxx[k].TotalBalance;
        classificationtypeLib = xxx[k].AccountClassification;

      }
      console.log(sumLib);
      console.log(classificationtypeLib);

      for (let j = 0; j < FINickNamearrLib.length; j++) {
        var y = _.filter(this.AllAssetNodesLib[i], function (acc) {
          return acc.FINickName === FINickNamearrLib[j];
        });


        this.InvestmentNodesbyFInicknameLib.push(y);
        console.log(this.InvestmentNodesbyFInicknameLib);
      }
      this.arrayItemsLib.push({ HeaderValue: classificationtypeLib, SumOFAmount: sumLib, PushedItems: this.InvestmentNodesbyFInicknameLib });
      this.InvestmentNodesbyFInicknameLib = [];
    }
    console.log(this.arrayItemsLib);


    //code to display asset chart
    var assetChartDataitem:number[];
    assetChartDataitem=[];
    for (let n = 0; n < this.arrayItems.length; n++) {
      this.assetChartLabels.push(this.arrayItems[n].HeaderValue);  
      assetChartDataitem.push(this.arrayItems[n].SumOFAmount);
      this.assetSum+=this.arrayItems[n].SumOFAmount;
    } 
    this.assetChartData=[]; 
    this.assetChartData.push(assetChartDataitem);

    //Code to display liabilities chart
    var liabilitiesChartDataitem:number[];
    liabilitiesChartDataitem=[];
    for (let m = 0; m < this.arrayItemsLib.length; m++) {
      this.liabilitiesChartLabels.push(this.arrayItemsLib[m].HeaderValue);  
      liabilitiesChartDataitem.push(this.arrayItemsLib[m].SumOFAmount);
      this.liabilitiesSum+=this.arrayItemsLib[m].SumOFAmount;
    }  
    this.liabilitiesChartData=[];
    this.liabilitiesChartData.push(liabilitiesChartDataitem);

    this.displayNetworth=this.assetSum-this.liabilitiesSum;
    
  }

  //code to display middle text in chart
  ngAfterViewInit() {
    let that=this;
    this.assetChartPlugins= [{
      beforeDraw(chart) {
        const ctx = chart.ctx!;
        ctx.textAlign = 'center';
      ctx.textBaseline = 'middle'
        //write your code 
        const centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      const centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      ctx.font = '15px Arial';
      ctx.fillStyle = 'blue';
      var vala = "$"+that.assetSum.toFixed(2);
      ctx.fillText(vala, centerX, centerY);
      }
    }]

    this.liabilitiesChartPlugins= [{
      beforeDraw(chart) {
        const ctx = chart.ctx!;
        ctx.textAlign = 'center';
      ctx.textBaseline = 'middle'
        //write your code 
        const centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
      const centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
      ctx.font = '15px Arial';
      ctx.fillStyle = 'blue';
      var vall = "$"+that.liabilitiesSum.toFixed(2);
        ctx.fillText(vall, centerX, centerY);
      }
    }]
  }

  uniqueBy(arr: any, prop: any) {
    return arr.reduce((a: any, d: any) => {
      if (!a.includes(d[prop])) { a.push(d[prop]); }
      return a;
    }, []);
  }


}
