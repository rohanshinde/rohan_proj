import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';


@Component({
  selector: 'custom-add-account-status',
  templateUrl: './add-account-status.component.html',
  styleUrls: ['./add-account-status.component.css']
})
export class AddAccountStatusComponent implements OnInit {

  AddAccountStatusMessage:string;

  constructor(private _http: HttpService, private _repository: ValueRepositoryService) {   
   this.AddAccountStatusMessage=this._repository.AddAccountStatusMessage;
  }

  
  ngOnInit(): void {
  }
 
}
