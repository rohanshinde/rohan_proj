import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccountStatusComponent } from './add-account-status.component';

describe('AddAccountStatusComponent', () => {
  let component: AddAccountStatusComponent;
  let fixture: ComponentFixture<AddAccountStatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAccountStatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccountStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
