import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTgzMTYwNTgsImV4cCI6MTYxODMxOTY1OCwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJEQUQyOUQ1QTkyMUM1QkE1OUI1MjMzQTAxOEU3NEY1RSIsImlhdCI6MTYxODMxNjA1Niwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.BNkgrS7Nno5NmxsCsoXEa62gcz9UZnUaGxrtDA_yqU2rlE2ZbSPmJ4BOn826_FpkDl3KMT2DyF3VSSTN3HRa70HWj5eDPxWm_vB1N8SeztjOMQ7VKO8fKQ-HT-dfHH8MDzJawSHjcwoAdYhvtNRzPQ64XYVDr4lTzjFe0pDNEzRmZg2CybQkoMfNtDYCJWE3PM8XDu_5LJoV4tXdm4ubuzpKkcH-dgSP6esmhmVQgob3gjISiPGl4Z9KNsXV_5X2aiz4So-jBTxrhMcVgm6uXvRx8VRowyGjPitF4CrI-O8PiDuoxzDhL3ZKNNsmu6Y3tFLUCV8emJuD6XF-sHeGVg";
const session="714730434a4b63446a412b562f5636642b69686b51513d3d";

@Component({
  selector: 'custom-add-account-challengequestion',
  templateUrl: './add-account-challengequestion.component.html',
  styleUrls: ['./add-account-challengequestion.component.css']
})

export class AddAccountChallengequestionComponent implements OnInit {

  ParameterList: {
    ParamName: string;
    ParamVal: string;   
  }[];
 

  ChallengeQuestions:any[];

  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) {
    
    this.ParameterList = [];
    //Get Challenge Question array from repository
    this.ChallengeQuestions=_repository.getchallengequestions();
    //this.ChallengeQuestions=[
    //  {
    //    "QuestionName": "answer",
    //    "QuestionDisplay": "What is your favourite Game ?Answer : baseball"
    //  }];

    for (let j = 0; j < this.ChallengeQuestions.length; j++) {
      this.ParameterList.push({ParamName:this.ChallengeQuestions[j].QuestionName,ParamVal:""});
    }
   }
   
  ngOnInit(): void {
  }

  onSubmit() {
    this.SpinnerService.show();
    console.log("Answer parameter list ---->"+this.ParameterList);
    var accounttoken=this._repository.getaccounttoken();
    console.log("Account token is"+accounttoken);
    accounttoken.ParameterList=this.ParameterList;

    this._http.addAccount(token,accounttoken).subscribe(data => {
      console.log("*********************Inside Challenge question..****************************");
      console.log("Harvest Id is.."+data.HarvestID);
      console.log("process code is.."+data.ProcessCode);
      console.log("process code is.."+JSON.stringify(data.HarvestFetchAccounts));
     this._repository.setaccounttoken(data);
     this.SpinnerService.hide();  
  });
  }

}
