import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccountChallengequestionComponent } from './add-account-challengequestion.component';

describe('AddAccountChallengequestionComponent', () => {
  let component: AddAccountChallengequestionComponent;
  let fixture: ComponentFixture<AddAccountChallengequestionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddAccountChallengequestionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccountChallengequestionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
