import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AqAccountDetailComponent } from './aq-account-detail.component';

describe('AqAccountDetailComponent', () => {
  let component: AqAccountDetailComponent;
  let fixture: ComponentFixture<AqAccountDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AqAccountDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AqAccountDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
