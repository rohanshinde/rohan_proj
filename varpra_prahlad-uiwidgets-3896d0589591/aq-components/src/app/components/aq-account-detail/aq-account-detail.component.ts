import { Component,Input, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { Account, AccountList,FiAccountDataList,Transaction, TransactionList } from 'src/app/models/accountList'
import { NgxSpinnerService } from "ngx-spinner";

import * as _ from 'lodash';

const token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTkxNTA2MjEsImV4cCI6MTYxOTE1NDIyMSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiJBRUUxREM2NzFFMUZERUI3RkM3MERFOTdBQTY1RUYzRSIsImlhdCI6MTYxOTE1MDYyMSwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.Av2whkHFA69tbqiBp0YrdSrM_tyVujACVvC7nGUp_ry6fBJD_ok5TymB37lciVp3cBVDxMq1RbGO2vuptBC3m30Yu58XFT3jueTTbRfW-c8bus3bOfRq0artsr4EG8W6M8qCegvDMKo5Q4oeYa-X032AujLNJ54tm3CjfJRlefDEjfxmIbYomXV_eM8jzX4m963Y1VpfcQn17W8rM3eoV_ZGFbcZAsM8Cwt1bq5eiY2L-yQusDjpB9OCNzbIo_eL5rrgr_Mep_vkpSRQWpvxpKERpF9J4GJ6ynJcELDYHWvRFoXtF6fP16UYm56aYrtpvy6xv_uuAtkfjymaH-jdbA";
const sessionid="71684b2f4833574177505765343152613478644565413d3d";

@Component({
  selector: 'custom-aq-account-detail',
  templateUrl: './aq-account-detail.component.html',
  styleUrls: ['./aq-account-detail.component.css']
})
export class AqAccountDetailComponent implements OnInit {
  //@Input() accountid:string="";

  accounts: Account[];
  transactions:Transaction[]
  AccountDetails: Account[];


  //Div active inactive variables
  AccountIsAsset:boolean=true;
  divPosition: boolean = true;
  divTransactions: boolean = false;
  divOtherBalance: boolean = false;
  bntStyle = "tablinks active";
  bntStyle2 = "tablinks";
  bntStyle3 = "tablinks";

  IsBankingOrLiabilities:boolean=true;
  IsAssetShowCost:boolean=true;
  DEPOSITORPAYMENT:string="";
  WITHDRAWALORCHARGES:string="";

  //date variables
  fromdatevalue = new Date();
  todatevalue=new Date();
  maxDate = new Date(); 

  NickNameAtFItoupdate:string;
  divNickNameatFILabel:boolean=true;
  divNicknameatFITextbox:boolean=false;
  fiaccountdatalist:FiAccountDataList[];
  AcctTypeId:string;
  ParameterList: {
    ParamName: string;
    ParamVal: string;   
  }[];


  constructor(private _http: HttpService, private _repository: ValueRepositoryService, private SpinnerService: NgxSpinnerService) {
    this.accounts = [];
    this.AccountDetails=[];
    this.transactions=[];

    this.maxDate.setDate(this.maxDate.getDate() - 90);
    this.fromdatevalue= this.maxDate;
    this.NickNameAtFItoupdate="";
    this.fiaccountdatalist=[]; 
    this.AcctTypeId="";   
    this.ParameterList = [];
   }

  ngOnInit(): void {
    console.log("Inside Init");
    console.log("Inside Init total is"+this._repository.getAccountID());
    this._repository.AccountIDChangeSubscriber.subscribe(data=>{
      if(data){
        this.GetAccounts();        
      }
    });
    
   // this.GetAccounts();
    
  }
  private GetAccounts() {
    console.log("Inside GetAccounts");
    this.SpinnerService.show();
    //let sessionid = this._repository.getSessionid();
    //let token = this._repository.getToken();

    this._http.getAccounts(token, sessionid).subscribe(data => {
      this._repository.setaccounts(data.Accounts);
      this.ProcessAccounts();      
    });  
  } 
  private ProcessAccounts() {
    console.log("inside Process accounts");
    this.accounts = this._repository.getaccounts();
    console.log(this.accounts);
    console.log("Account Id From repository"+this._repository.getAccountID());
    
    this.AccountDetails = _.filter(this.accounts,  (acc) => {
      return acc.AcctId === this._repository.getAccountID();
    });
    console.log("Account id "+this._repository.getAccountID());
    console.log("Account Detais are---->>> "+JSON.stringify(this.AccountDetails));

    if(this.AccountDetails[0].AssetType === 'Assets')
    {
      if(this.AccountDetails[0].AccountClassification==='Banking'){
        this.AccountIsAsset=false;
        this.divPosition = false;
        this.divTransactions = true;
        this.divOtherBalance = false;
        this.bntStyle = "tablinks";
        this.bntStyle2 = "tablinks active";
        this.bntStyle3="tablinks";        
        this.IsBankingOrLiabilities=true;
       this.DEPOSITORPAYMENT="DEPOSIT";
      this.WITHDRAWALORCHARGES="WITHDRAWAL";
      this.IsAssetShowCost=false;

      }else
      {
          this.AccountIsAsset=true;
          this.divPosition = true;
          this.divTransactions = false;
          this.divOtherBalance = false;
          this.bntStyle = "tablinks active";
          this.bntStyle2 = "tablinks";
          this.bntStyle3="tablinks";          
          this.IsBankingOrLiabilities=false;
          this.IsAssetShowCost=true;
      }
    }
    else if(this.AccountDetails[0].AssetType === 'Liabilities'){
      this.AccountIsAsset=false;
      this.divPosition = false;
      this.divTransactions = true;
      this.divOtherBalance = false;
      this.bntStyle = "tablinks";
      this.bntStyle2 = "tablinks active";
      this.bntStyle3="tablinks";     
      this.IsBankingOrLiabilities=true;
      this.DEPOSITORPAYMENT="PAYMENT";
      this.WITHDRAWALORCHARGES="CHARGES";
      this.IsAssetShowCost=false;
    }
    console.log(this.AccountDetails);

    //Get Transactions   
   this.GetTransactions();
  }
  GetTransactions(){
  this._http.getAccountTransactions(this.AccountDetails[0].AcctId,this.AccountDetails[0].AccountType,
    this.AccountDetails[0].ExtendedAccountType,this.AccountDetails[0].AccountAddedBy,token, sessionid,this.fromdatevalue,this.todatevalue).subscribe(data => {
    //this.ProcessAccountTransactions(data);
    this.transactions=data.Transactions;
    this.SpinnerService.hide();     
  });
}
 //private ProcessAccountTransactions(data: TransactionList) {   
  //  this.transactions=data.Transactions;
 //   console.log("inside process Transaction"+this.transactions)
 // }
  divPositionFunction() {
    this.divPosition = true;
    this.divTransactions = false;
    this.divOtherBalance = false;
    this.bntStyle = "tablinks active";
    this.bntStyle2 = "tablinks";
    this.bntStyle3="tablinks";
  }
  divTransactionsFunction() {
    this.divPosition = false;
    this.divTransactions = true;
    this.divOtherBalance = false;
    this.bntStyle = "tablinks";
    this.bntStyle2 = "tablinks active";
    this.bntStyle3="tablinks";
    
  }
  divOtherBalancesFunction() {
    this.divPosition = false;
    this.divTransactions = false;
    this.divOtherBalance = true;
    this.bntStyle = "tablinks";
    this.bntStyle2 = "tablinks";
    this.bntStyle3="tablinks active";
  }
  TickerClick(headervalue:string){
    alert(headervalue);    
  }
  fromdateChanged($event:any){ 
        this.fromdatevalue=new Date($event.toJSON().split('T')[0]);  
        this.GetTransactions();  
        
  }
  todateChanged($event:any){       
        this.todatevalue=new Date($event.toJSON().split('T')[0]);  
        this.GetTransactions(); 
        
  }
  HideLabelShowTextbox(NickNameAtFItoupdate:string,AcctId:string){
    this.divNicknameatFITextbox=true;
    this.divNickNameatFILabel=false;    
    this.NickNameAtFItoupdate=NickNameAtFItoupdate;
  }
  HideTextBoxShowLabel(){
    this.divNicknameatFITextbox=false;
    this.divNickNameatFILabel=true;     
  }
  UpdateNicknameatFI(){   
    this._http.getFinancialInstitution(token,this.AccountDetails[0].FIId).subscribe(data => {      
      this.fiaccountdatalist=data.FinancialInstitution.FiAccountDataList; 
      
      var x = _.filter(this.fiaccountdatalist, (acc) => {
        return acc.AcctType === this.AccountDetails[0].AccountType && acc.ExtAcctType === this.AccountDetails[0].ExtendedAccountType;
      });    
      
      for (let a = 0; a < x.length; a++) { 
        this.AcctTypeId=x[a].AcctTypeId;
      }

      console.log("Account Type Id is "+this.AcctTypeId);
      this.ParameterList=[];

      if(this.AccountDetails[0].Instrument =="N/A" || this.AccountDetails[0].Instrument=="Not Selected")
      {
        this.ParameterList.push({ParamName:"Instrument",ParamVal:""});
        console.log("Instrument is if"+this.AccountDetails[0].Instrument);
      }else{
      this.ParameterList.push({ParamName:"Instrument",ParamVal:this.AccountDetails[0].Instrument});
        console.log("Instrument is else"+this.AccountDetails[0].Instrument);
      }

      if(this.AccountDetails[0].RetirementStatus =="N/A" || this.AccountDetails[0].RetirementStatus=="Not Selected")
      {
        this.ParameterList.push({ParamName:"RetirementStatus",ParamVal:""});
        console.log("RetirementStatus is if"+this.AccountDetails[0].RetirementStatus);
      }else
      {
        this.ParameterList.push({ParamName:"RetirementStatus",ParamVal:this.AccountDetails[0].RetirementStatus});
        console.log("RetirementStatus is else"+this.AccountDetails[0].RetirementStatus);
      }

      if(this.AccountDetails[0].AccountOwnership =="N/A" || this.AccountDetails[0].AccountOwnership=="Not Selected")
      {
      this.ParameterList.push({ParamName:"AccountOwnership",ParamVal:""});
      console.log("AccountOwnership is if"+this.AccountDetails[0].AccountOwnership);
      }else{
        this.ParameterList.push({ParamName:"AccountOwnership",ParamVal:this.AccountDetails[0].AccountOwnership});
        console.log("AccountOwnership is else"+this.AccountDetails[0].AccountOwnership);
      }

      if(this.AccountDetails[0].AdvAccess =="N/A" || this.AccountDetails[0].AdvAccess=="Not Selected")
      {
      this.ParameterList.push({ParamName:"AdvAccess",ParamVal:""});
      console.log("AdvAccess is if"+this.AccountDetails[0].AdvAccess);
      }
      else{
        this.ParameterList.push({ParamName:"AdvAccess",ParamVal:this.AccountDetails[0].AdvAccess});
        console.log("AdvAccess is else"+this.AccountDetails[0].AdvAccess);
      }

      console.log("account id is "+this.AccountDetails[0].AcctId);
      console.log("Account Type is"+this.AccountDetails[0].AccountType);
      console.log("account type id is "+this.AcctTypeId);
      console.log(" Extende Account Type is"+this.AccountDetails[0].ExtendedAccountType);
      console.log("FIID  id is "+this.AccountDetails[0].FIId);
      console.log("Nicknam to update  Type is"+this.NickNameAtFItoupdate);
      console.log(JSON.stringify(this.ParameterList));

      this._http.ModifyAggregateAccountProperty(token,this.AccountDetails[0].AcctId,this.AccountDetails[0].AccountType,
        this.AcctTypeId,"USD",this.AccountDetails[0].ExtendedAccountType,this.AccountDetails[0].FIId,
        this.NickNameAtFItoupdate,this.ParameterList,sessionid).subscribe(data1=>{
          if(data1){
            this.GetAccounts(); 
            this.HideTextBoxShowLabel();  
          }
          
      });
    });
  }
  uniqueBy(arr: any, prop: any) {
    return arr.reduce((a: any, d: any) => {
      if (!a.includes(d[prop])) { a.push(d[prop]); }
      return a;
    }, []);
  }
}
