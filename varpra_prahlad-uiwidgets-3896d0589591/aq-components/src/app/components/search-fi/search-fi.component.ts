import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { ValueRepositoryService } from 'src/app/Services/value-repository.service';
import { FinancialInstitution } from 'src/app/models/accountList'
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'search-fi',
  templateUrl: './search-fi.component.html',
  styleUrls: ['./search-fi.component.css']
})
export class SearchFiComponent implements OnInit {
 
  financialinstitutions: FinancialInstitution[];

  //keyword:string;
  constructor(private _http: HttpService, private _repository: ValueRepositoryService,private SpinnerService: NgxSpinnerService) {
   // this.keyword="";
    this.financialinstitutions=[];    
   }

  ngOnInit(): void {
  }

  onChangeEvent(event: any){

    this.SpinnerService.show();
    console.log(event.target.value);

    let token = "eyJhbGciOiJSUzI1NiIsImtpZCI6IkI3NEUxQjU3NTAxMjA1RTU3QzBEQUJCQTlCMjJCRDAxIiwidHlwIjoiSldUIn0.eyJuYmYiOjE2MTc4NTI2MjEsImV4cCI6MTYxNzg1NjIyMSwiaXNzIjoiaHR0cHM6Ly9hdXRoLmFxdW11bGF0ZS5jb20iLCJhdWQiOiJodHRwczovL2F1dGguYXF1bXVsYXRlLmNvbS9yZXNvdXJjZXMiLCJjbGllbnRfaWQiOiJhcXVtdWxhdGUiLCJqdGkiOiI1N0JBQkFCMkIyMjU0MzBBMEY1RDYxN0ZCNzkyQkJGMyIsImlhdCI6MTYxNzg1MjYyMCwic2NvcGUiOlsidWl3aWRnZXRhcGkiXX0.tO5hHYRSt6RRHVlOIVbSXwYAIb04pdBWfG8nCXU3QrWR_W6A6AR-NT3sor9xvunOjXFVcBnb4L5Qy8ff-M1lm0JH3yMWxowYvNLyERwMgZaJA-tVlsoXn_0RWIDuv3bwtpikqDZl8JfYvoDIVgG45F25pYZ_8LsAziEETx6w8iKJoHVzVJmJ_HciqBotGC2DdORz1wQesVe1eNUOB_HcLrBF2MKzzTltVAiK_rfDXUgHlgSL2J2hl1cWKUFHWcSYbXyONE0K1_Xgw-2NQOZRfh0qq3yM_VNTS9Ey9v7urfwGVj9J421ZGGlUl2Kd6gF5bi1_waK9yL-dY3yRMKPR6g";
    console.log("Inside List Accounts token is" + token);
    this._http.searchFinancialInstitution(token,event.target.value).subscribe(data => {
     this.financialinstitutions = data.FinancialInstitutions;   
     console.log(this.financialinstitutions); 
     this.SpinnerService.hide();  
    });
   //console.log(this.keyword);
   console.log(this.financialinstitutions);

  }
  loadLoginComponent(fiid:number){ 
    this._repository.setStep('2'); 
    this._repository.setFIID(fiid.toString());      
    console.log("FIIIID"+this._repository.getFIID());
 }

}
