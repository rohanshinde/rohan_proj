import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFiComponent } from './search-fi.component';

describe('SearchFiComponent', () => {
  let component: SearchFiComponent;
  let fixture: ComponentFixture<SearchFiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SearchFiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
