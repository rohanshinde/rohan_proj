import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TestComponent } from './components/test/test.component';

import  { createCustomElement } from '@angular/elements';
import { AqEnvComponent } from './components/aq-env/aq-env.component';
import {HttpClientModule} from '@angular/common/http';
import { SearchFiComponent } from './components/search-fi/search-fi.component';
import { EnterCredentialsComponent } from './components/enter-credentials/enter-credentials.component';
import { AddAccountStatusComponent } from './components/add-account-status/add-account-status.component';
import { AddAccountFlowComponent } from './components/add-account-flow/add-account-flow.component'
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { ListAccountsComponent } from './components/list-accounts/list-accounts.component';
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AqTopholdingsComponent } from './components/aq-topholdings/aq-topholdings.component';
import { AqNetworthChartComponent } from './components/aq-networth-chart/aq-networth-chart.component';
import { GoogleChartsModule } from 'angular-google-charts';
import { ChartsModule } from 'ng2-charts';
import { AqAccountDetailComponent } from './components/aq-account-detail/aq-account-detail.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { AddAccountAccountclassificationComponent } from './components/add-account-accountclassification/add-account-accountclassification.component';
import { AddAccountChallengequestionComponent } from './components/add-account-challengequestion/add-account-challengequestion.component';

@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    AqEnvComponent,
    SearchFiComponent,
    EnterCredentialsComponent,
    AddAccountStatusComponent,
    AddAccountFlowComponent,
    ListAccountsComponent,
    AqTopholdingsComponent,
    AqNetworthChartComponent,
    AqAccountDetailComponent,
    AddAccountAccountclassificationComponent,
    AddAccountChallengequestionComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    GoogleChartsModule,
    ChartsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [],
  bootstrap: [],
  entryComponents:[
    TestComponent, 
    AqEnvComponent,
    AddAccountFlowComponent,
    ListAccountsComponent,
    AqNetworthChartComponent,
    AqTopholdingsComponent,
    AqAccountDetailComponent,
    SearchFiComponent,
    EnterCredentialsComponent,
    AddAccountStatusComponent,
    AddAccountAccountclassificationComponent,
    AddAccountChallengequestionComponent
  ]
})
export class AppModule {

  constructor(private injector : Injector){}
  ngDoBootstrap(){
      const aq_test = createCustomElement(TestComponent, {injector : this.injector});
      customElements.define('aq-test',aq_test);

      const aq_env=createCustomElement(AqEnvComponent, {injector : this.injector});
      customElements.define('aq-env',aq_env);
      const add_account=createCustomElement(AddAccountFlowComponent, {injector : this.injector});
      customElements.define('add-account-flow',add_account);
      
      
      const list_accounts=createCustomElement(ListAccountsComponent, {injector : this.injector});
      customElements.define('list-accounts',list_accounts);       

      const aq_networthchart=createCustomElement(AqNetworthChartComponent, {injector : this.injector});
      customElements.define('custom-aq-networth-chart',aq_networthchart);
      
      const aq_Topholdings=createCustomElement(AqTopholdingsComponent, {injector : this.injector});
      customElements.define('custom-aq-topholdings',aq_Topholdings);

      const aq_AccountDetail=createCustomElement(AqAccountDetailComponent, {injector : this.injector});
      customElements.define('custom-aq-account-detail',aq_AccountDetail);

      const aq_searchFi=createCustomElement(SearchFiComponent, {injector : this.injector});
      customElements.define('search-fi',aq_searchFi);

      const aq_EnterCredentialsComponent=createCustomElement(EnterCredentialsComponent, {injector : this.injector});
      customElements.define('custom-enter-credentials',aq_EnterCredentialsComponent);

      const aq_AddAccountStatusComponent=createCustomElement(AddAccountStatusComponent, {injector : this.injector});
      customElements.define('custom-add-account-status',aq_AddAccountStatusComponent);
      //custom-add-account-accountclassification
      //AddAccountAccountclassificationComponent

      const aq_AddAccountAccountclassificationComponent=createCustomElement(AddAccountAccountclassificationComponent, {injector : this.injector});
      customElements.define('custom-add-account-accountclassification',aq_AddAccountAccountclassificationComponent);
      //AddAccountChallengequestionComponent
      const aq_AddAccountChallengequestionComponent=createCustomElement(AddAccountChallengequestionComponent, {injector : this.injector});
      customElements.define('custom-add-account-challengequestion',aq_AddAccountChallengequestionComponent);
     
  }
 }
