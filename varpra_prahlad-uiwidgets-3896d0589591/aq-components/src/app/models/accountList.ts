export interface Account{
    FILoginAcctId:string;
    AccountAddedBy: string;
    FIId:string;
    FIName:string;
    AcctId:string;
    AccountType:string;
    ExtendedAccountType:string;
    NickNameAtFI: string;
    LastUpdateStatusCode: string;
    UpdateErrorCode: string;
    LastUpdateStatusMsg: string;
    LastUpdateStatusMessageAtFI: string;
    LastUpdateAttempt: string;
    RetirementStatus: string;
    Instrument: string;
    AccountOwnership: string,
    AdvAccess:string;
    TrackingCode: string;
    LastSuccessfulUpdate: string;
    AccountBalances:AccountBalance[];
    Positions:Position[];
    AccountNumber:string;
    FINickName: string;
    AccountClassification: string,
    TotalBalance:number;
    AssetType: string;
    AssetOrder: number
}

export interface AccountList{
    Accounts:Account[];
    Status: number;
    ErrorMessage:string;
    ExtendedErrorMessage:string;
    ExtendedErrorCode:string;
}
export interface AccountBalance{
    AcctBalType: string;
    AcctBalAmt: number;
    CurCode: string
}
export interface Position{
    AcctId: string;
    Date: string;
    PosID: string;
    Ticker: string;
    Description: string;
    AssetID: string;
    AssetIDType: string;
    Price: number;
    Quantity: number;
    MarketValue: number;
    CostBasis: number
}
export interface Transaction{
    AccountId: string;
    TransactionId: string;
    TransactionType: string;
    PostedDate: string;
    OriginationDate: string;
    Amount: number;
    CurCode: string;
    Description: string;
    TransactionCode: string;
    TransactionAction: string;
    TransactionDate: string;
    Commission: number;
    Ticker: string;
    PosDescription: string;
    AssetID: string;
    AssetIDType: string;
    Units:number;
    Price: number;
    CheckNumber: string;
    CreatedOn: string;
}
export interface TransactionList{
    Transactions:Transaction[];
    Status: number;
    ErrorMessage: string;
    ExtendedErrorMessage: string;
    ExtendedErrorCode: string;
    
}
export interface FinancialInstitution{
    FIId:number;
    FIName:string;  
    Url:string;  
    FILoginParameters:FILoginParameter[];
    FiAccountDataList:FiAccountDataList[];
}
export interface FinancialInstitutionsList{
    FinancialInstitutions:FinancialInstitution[];
    Status:string;
    ErrorMessage: string;
    ExtendedErrorMessage: string;
    ExtendedErrorCode: string;
}
export interface FILoginParameter{
    ParameterId: number;
    ParameterNumber: number;
    ParameterType: string;
    ParameterMaxLength: number;
    ParameterSize: number;
    ParameterCaption: string;
    ParameterVariableName: string;
    ParameterDefaultValue: string;
    ParameterEditable: true;
    ParameterSensitivityCode: string;
}
export interface FiAccountDataList{
    AcctTypeId: string;
    AcctGroup: string;
    AcctName: string;
    AcctType: string;
    ExtAcctType: string;
}
export interface FinancialInstitutionByID{    
    FinancialInstitution:FinancialInstitution;
    Status:string;
    ErrorMessage: string;
    ExtendedErrorMessage: string;
    ExtendedErrorCode: string;
}

export interface AccountToken{
    ProcessCode: string;
    FIId: number;
    ParameterList: ParameterList[];
    HarvestID: string;
    LoginAccountId: string;
    SessionId: string;
    UpdateErrorCode: string;
    UpdateErrorMessage: string;
    ExtendedUpdateErrorMessage: string;
    HarvestStatus: string;
    CEUserId: string;
    Continue: boolean;
    ChallengeQuestions: ChallengeQuestion[];
    HarvestFetchAccounts: HarvestFetchAccount[],
    AccountList: string;
    Status: number;
    ErrorMessage: string;
    ExtendedErrorMessage: string;
    ExtendedErrorCode: string;

}
export interface ChallengeQuestion{
    QuestionName: string;
    QuestionDisplay: string;
}
export interface HarvestFetchAccount{
    FIId: number;
    AccountNumber: string;
    NickNameAtFI: string;
    AccountTypeId: number;
    CurrencyCode: string;
    AccountBalance: number;
    Misc: string;
    AdvAccess: string;
    AccountOwnerShip: string;
    RetirementStatus: string;
    Instrument: string;
}
export interface ParameterList{
    ParamName: string;
    ParamVal: string;
}