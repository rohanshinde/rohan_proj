import { TestBed } from '@angular/core/testing';

import { ValueRepositoryService } from './value-repository.service';

describe('ValueRepositoryService', () => {
  let service: ValueRepositoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValueRepositoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
