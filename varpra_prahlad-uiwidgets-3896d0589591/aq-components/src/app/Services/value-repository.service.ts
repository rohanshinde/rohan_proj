import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Account, AccountToken, ChallengeQuestion, FiAccountDataList, HarvestFetchAccount } from 'src/app/models/accountList'

@Injectable({
  providedIn: 'root'
})
export class ValueRepositoryService {

  accountid:string;
  FIID:string;
  Step:string;
  accounts: Account[];
  accounttoken!: AccountToken;

  //Account type and harvest accounts
  fiaccountdatalist:FiAccountDataList[];
  harvestfetchaccount:HarvestFetchAccount[];
  challengequestions:ChallengeQuestion[];
  //Status Message
  AddAccountStatusMessage:string;

  observer=new Subject();
  observerFIID=new Subject();
  observerStep=new Subject();
  observeraccounttoken=new Subject();

  public AccountIDChangeSubscriber=this.observer.asObservable();
  public FIIDChangeSubscriber=this.observerFIID.asObservable();
  public StepChangeSubscriber=this.observerStep.asObservable();
  public AccountTokenChangeSubscriber=this.observeraccounttoken.asObservable();

constructor() { 
  this.accountid="";
  this.FIID="";
  this.Step="";
  this.accounts = [];  
  this.AddAccountStatusMessage="";
  this.fiaccountdatalist=[];
  this.harvestfetchaccount=[];
  this.challengequestions=[];
  }
  setaccounts(_accounts: Account[]){
    this.accounts=_accounts
  }
  getaccounts(){
    return this.accounts;
  }

  setaccounttoken(_accounttoken: AccountToken){
    this.accounttoken=_accounttoken;
    this.observeraccounttoken.next(true);
  }
  getaccounttoken(){
    return this.accounttoken;
  }

  setfiaccountdatalist(_fiaccountdatalist: FiAccountDataList[]){
    this.fiaccountdatalist=_fiaccountdatalist;    
  }
  getfiaccountdatalist(){
    return this.fiaccountdatalist;
  }

  setharvestfetchaccount(_harvestfetchaccount: HarvestFetchAccount[]){
    this.harvestfetchaccount=_harvestfetchaccount;    
  }
  getharvestfetchaccount(){
    return this.harvestfetchaccount;
  }

  setchallengequestions(_challengequestions: ChallengeQuestion[]){
    this.challengequestions=_challengequestions;    
  }
  getchallengequestions(){
    return this.challengequestions;
  }
  getToken():string{ 
    return sessionStorage.getItem('TOKEN') || '';    
    
  }
  getSessionid():string{
    return sessionStorage.getItem('SESSION') || '';
  }
  
  setAccountID(_accountid:string):void{
    this.accountid=_accountid;
    this.observer.next(true);
  }
  getAccountID():string{
    return this.accountid;
  }
  setFIID(_fiid:string){
    this.FIID=_fiid;
    this.observerFIID.next(true);
  }
  getFIID():string{
    return this.FIID;
  }

  setStep(_step:string){
    this.Step=_step;
    this.observerStep.next(true);
  }
  getStep():string{
    return this.Step;
  }
}
