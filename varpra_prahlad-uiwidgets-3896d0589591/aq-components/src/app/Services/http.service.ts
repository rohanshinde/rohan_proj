import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AccountList, AccountToken,FinancialInstitutionByID, FinancialInstitutionsList, TransactionList } from '../models/accountList';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  
  baseAddress:string;
  constructor(private _http:HttpClient) { 
    this.baseAddress="https://ceapiv2.aqumulate.com/api/";
  }
  
  keepAliveSession(token:string, sessionid:string){
    let url=this.baseAddress+"users/KeepAliveClientSession";
    let headers={'Authorization':'Bearer '+token};
    let body={'UserID':"","Password":"","SessionId":sessionid}
    return this._http.post(url,body,{headers});
  }

  getAccounts(token:string, sessionid:string):Observable<AccountList>{
    let url=this.baseAddress+"Account/GetAccounts";
    let headers={'Authorization':'Bearer '+token};
    let body={"SessionID":sessionid,"ClientSSN":"","AccountID":""};    
    return this._http.post<AccountList>(url,body,{headers});
  }
  
  getAccountTransactions(accountid:string,accounttype:string,extendedaccounttype:string,accountAaddedby:string,token:string, sessionid:string,startdate:Date,enddate:Date):Observable<TransactionList>{
    let url=this.baseAddress+"Account/GetTransactions";
    let headers={'Authorization':'Bearer '+token};
    let body={"AccountInfoList":[{"AccountId":accountid,"AccountType":accounttype,"ExtendedAccountType":extendedaccounttype,"AccountAddedBy":accountAaddedby}],"SessionID":sessionid,"CEUserId":"","StartDate":startdate,"EndDate":enddate,"ClientSSN":""};    
    return this._http.post<TransactionList>(url,body,{headers});
  }
  searchFinancialInstitution(token:string,keyword:string):Observable<FinancialInstitutionsList>{
    let url=this.baseAddress+"Account/SearchFinancialInstitution";
    let headers={'Authorization':'Bearer '+token};
    let body={"Keyword":keyword};    
    return this._http.post<FinancialInstitutionsList>(url,body,{headers});
  }

  getFinancialInstitution(token:string,fiid:string):Observable<FinancialInstitutionByID>{
    let url=this.baseAddress+"Account/GetFinancialInstitution";
    let headers={'Authorization':'Bearer '+token};
    let body={"Fiid":fiid};    
    return this._http.post<FinancialInstitutionByID>(url,body,{headers});
  }

  addAccountInitiate(token:string,fiid:string,sessionid:string,parameterlist: any[]):Observable<AccountToken>{
    let url=this.baseAddress+"Account/AddAccount";
    let headers={'Authorization':'Bearer '+token}; 
    let body={
                    "ProcessCode": "",
                    "FIId": fiid,
                    "ParameterList": parameterlist,
                    "HarvestID": "",
                    "LoginAccountId": "",
                    "SessionId": sessionid,
                    "UpdateErrorCode": "",
                    "UpdateErrorMessage": "",
                    "ExtendedUpdateErrorMessage": "",
                    "HarvestStatus": "",
                    "CEUserId": "",
                    "Continue": true,
                    "ChallengeQuestions": [
                      {
                        "QuestionName": "",
                        "QuestionDisplay": ""
                      }
                    ],
                    "HarvestFetchAccounts": [
                      {
                        "FIId": 0,
                        "AccountNumber": "",
                        "NickNameAtFI": "",
                        "AccountTypeId": 0,
                        "CurrencyCode": "",
                        "AccountBalance": 0,
                        "Misc": "",
                        "AdvAccess": "",
                        "AccountOwnerShip": "",
                        "RetirementStatus": "",
                        "Instrument": ""
                      }
                    ],
                    "AccountList": "",
                    "Status": 0,
                    "ErrorMessage": "",
                    "ExtendedErrorMessage": "",
                    "ExtendedErrorCode": ""

                  }
    return this._http.post<AccountToken>(url,body,{headers});
  }
   
  addAccount(token:string,accounttoken:AccountToken):Observable<AccountToken>{
    let url=this.baseAddress+"Account/AddAccount";
    let headers={'Authorization':'Bearer '+token}; 
    let body={
                "ProcessCode": accounttoken.ProcessCode,
                "FIId": accounttoken.FIId,
                "ParameterList": accounttoken.ParameterList,
                "HarvestID": accounttoken.HarvestID,
                "LoginAccountId": accounttoken.LoginAccountId,
                "SessionId": accounttoken.SessionId,
                "UpdateErrorCode": accounttoken.UpdateErrorCode,
                "UpdateErrorMessage":accounttoken.UpdateErrorMessage,
                "ExtendedUpdateErrorMessage": accounttoken.ExtendedUpdateErrorMessage,
                "HarvestStatus": accounttoken.HarvestStatus,
                "CEUserId": accounttoken.CEUserId,
                "Continue": true,
                "ChallengeQuestions": accounttoken.ChallengeQuestions,
                "HarvestFetchAccounts": accounttoken.HarvestFetchAccounts,
                "AccountList": "",
                "Status": accounttoken.Status,
                "ErrorMessage": accounttoken.ErrorMessage,
                "ExtendedErrorMessage": accounttoken.ExtendedErrorMessage,
                "ExtendedErrorCode": accounttoken.ExtendedErrorCode
            }
    return this._http.post<AccountToken>(url,body,{headers});
  }

  setClientFIMapping(token:string, userceid:string,fiid:string,finickname:string):Observable<any>{
    let url=this.baseAddress+"Account/SetClientFIMapping";
    let headers={'Authorization':'Bearer '+token};
    let body={"UserCEID":userceid,"FIID":fiid,"FINickName":finickname,"Method":"UPDATE"};    
    return this._http.post<any>(url,body,{headers});
  }

  ModifyAggregateAccountProperty(token:string, accountid:string,accounttype:string,accounttypeid:string,
    currencycode:string,extendedaccounttype:string,fiid:string,nickname:string,propertylist:any[],sessionid:string):Observable<any>{
    let url=this.baseAddress+"Account/ModifyAggregateAccountProperty";
    let headers={'Authorization':'Bearer '+token};
    let body={"AccountId":accountid,
              "AccountType":accounttype,
              "AccountTypeID":accounttypeid,
              "CurrencyCode":currencycode,
              "ExtendedAccountType":extendedaccounttype,
              "FIID":fiid,
              "NickName":nickname,
              "PropertyList":propertylist,
              "SessionId":sessionid
            }; 
            console.log("Inside services body is -->>"+JSON.stringify(body));   
    return this._http.post<any>(url,body,{headers});
  }

}
